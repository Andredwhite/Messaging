//
//  UserInfoViewController.m
//  IDMessage
//
//  Created by Andre White on 7/19/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "UserInfoViewController.h"

@interface UserInfoViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITextField *displayNameField;
@property (weak, nonatomic) IBOutlet UILabel *takenLabel;

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _displayNameField.delegate=self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)screenTapped:(id)sender {
    if ([_displayNameField isFirstResponder]) {
        [_displayNameField resignFirstResponder];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (_takenLabel.hidden==NO) {
        _takenLabel.hidden=YES;
    }
    return YES;
}
- (IBAction)setInfo:(id)sender {
    if (_displayNameField.text.length!=0) {
        [AppManager findFriend:_displayNameField.text withCompletion:^(NSString *userID) {
            if(userID.length>0){
                _takenLabel.hidden=NO;
                _displayNameField.text=@"";
                
            }
            else{
                FIRUserProfileChangeRequest* request=[[FIRAuth auth].currentUser profileChangeRequest];
                request.displayName=_displayNameField.text;
                [request commitChangesWithCompletion:^(NSError * _Nullable error) {
                    if (!error) {
                        [[AppManager UserNamesReferenceForUserWithID:[AppManager UserID]]setValue:request.displayName];
                        [[AppManager UserIDReferenceForUserWithUserName:request.displayName]setValue:[AppManager UserID]];
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                    else{
                        NSLog(@"%@", error.localizedDescription);
                    }
                }];
            }
        }];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  MessageSession.m
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "MessageSession.h"

@import FirebaseDatabase;
@interface MessageSession ()
@property(retain, nonatomic)NSMutableArray* messages;
@property(retain, nonatomic)JSQMessagesBubbleImage* outgoingBubble;
@property(retain, nonatomic)JSQMessagesBubbleImage* incomingBubble;
@property(retain, nonatomic)JSQMessagesAvatarImage* outgoingAvatar;
@property(retain, nonatomic)FIRDatabaseReference* convoRef;
@property(retain, nonatomic)FIRStorageReference* convoStorageRef;
@property(retain, nonatomic)NSMutableArray* messageCache;
@property(retain, nonatomic)NSString* displayName;
@end
@implementation MessageSession
-(instancetype)initWithConvoKey:(NSString *)convoKey{
    self=[super init];
    if (self) {
        _messageCache=[[NSMutableArray alloc]init];
        _messages=[[NSMutableArray alloc]init];
        _convoRef=[AppManager ConversationReferenceWithKey:convoKey];
        _convoStorageRef=[AppManager ConversationStorageReferenceWithKey:convoKey];
        FIRDatabaseHandle handle=[_convoRef observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (![_messageCache containsObject:snapshot.key]) {
                [_messages addObject:[[Message alloc]initWithDictionary:snapshot.value]];
                [_messageCache removeObject:snapshot.key];
                [self sortMessages];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"New Message" object:nil];
            }
            else{
                [_messageCache removeObject:snapshot.key];
            }
        }];
        [[AppManager sharedInstance]addHandle:handle ForKey:[NSString ConversationsHandleKey]];
        JSQMessagesBubbleImageFactory* bubbleFactory=[[JSQMessagesBubbleImageFactory alloc]init];
        _outgoingBubble=[bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:.5 green:.24 blue:.7 alpha:1]];
        _incomingBubble=[bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:.25 green:.24 blue:.7 alpha:1]];
        _outgoingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[NSString stringWithFormat:@"%c",[[self displayName] characterAtIndex:0]] backgroundColor:[UIColor colorWithWhite:.5 alpha:1] textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:14] diameter:34];
        //_incomingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[NSString stringWithFormat:@"%c",[[self displayName] characterAtIndex:0]] backgroundColor:[UIColor colorWithWhite:.5 alpha:1] textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:14] diameter:34];
    }
    return self;
}
-(void)sortMessages{
    NSSortDescriptor* desc=[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    [_messages sortUsingDescriptors:@[desc]];
}
-(instancetype)initWithDisplayName:(NSString*)displayName{
    self=[super init];
    if (self) {
        _displayName=displayName;
        _messages=[[NSMutableArray alloc]init];
        JSQMessagesBubbleImageFactory* bubbleFactory=[[JSQMessagesBubbleImageFactory alloc]init];
        _outgoingBubble=[bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:.5 green:.24 blue:.7 alpha:1]];
        _incomingBubble=[bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:.25 green:.24 blue:.7 alpha:1]];
        _outgoingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[NSString stringWithFormat:@"%c",[[self displayName] characterAtIndex:0]] backgroundColor:[UIColor colorWithWhite:.5 alpha:1] textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:14] diameter:34];
        //_incomingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[NSString stringWithFormat:@"%c",[_displayName characterAtIndex:0]] backgroundColor:[UIColor colorWithWhite:.5 alpha:1] textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:14] diameter:34];
    }
    return self;
}
-(void)deleteMessageAtIndex:(NSInteger)index{
    [_messages removeObjectAtIndex:index];
}
-(void)updateMessage:(Message*) aMessage withImage:(UIImage*)image{
    [_messages indexOfObject:aMessage];
    
}
-(void)addMessage:(NSDictionary *)theMessage{
    if (![theMessage[@"media"] isKindOfClass:[NSString class]]) {
        NSString* messageKey=[[_convoRef childByAutoId]key];
        JSQPhotoMediaItem* media=theMessage[@"media"];
        Message* aMessage=[[Message alloc]initWithMediaDictionary:theMessage];
        [_messageCache addObject:messageKey];
        [_messages addObject:aMessage];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"New Message" object:nil];
        
        FIRStorageUploadTask* task=[[_convoStorageRef child:messageKey] putData:UIImageJPEGRepresentation(media.image, 1.0) metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
            if (!error) {
                NSMutableDictionary* dic=[NSMutableDictionary dictionaryWithDictionary:theMessage];
                dic[@"media"]=metadata.downloadURL.absoluteString;
                [[_convoRef child:messageKey]setValue:dic];
            }
            else{
                NSLog(@"%@", error.localizedDescription);
            }
        }];
    }
    else{
        [[_convoRef childByAutoId]setValue:theMessage];
    }
}
-(JSQMessage*)messageAtIndex:(NSInteger)index{
    return [_messages objectAtIndex:index];
}
-(JSQMessagesBubbleImage*)messageBubbleForIndex:(NSInteger)index{
    JSQMessage* message=[_messages objectAtIndex:index];
    
    if ([message.senderId isEqualToString:[AppManager UserID]]) {
        return _outgoingBubble;
    }
    else{
        return _incomingBubble;
    }
}
-(JSQMessagesAvatarImage*)avatarForIndex:(NSInteger)index{
    JSQMessage* message=[_messages objectAtIndex:index];
    if ([message.senderId isEqualToString:[AppManager UserID]]) {
        return _outgoingAvatar;
    }
    else{
        return [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[NSString stringWithFormat:@"%c",[[self receiverDisplayName] characterAtIndex:0]] backgroundColor:[UIColor colorWithWhite:.5 alpha:1] textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:14] diameter:34];
    }
}
-(NSInteger)messageCount{
    return _messages.count;
}

-(NSString*)receiverDisplayName{
    NSMutableArray* array=[NSMutableArray array];
    for (Message* aMessage in _messages) {
        if (![array containsObject:aMessage.senderDisplayName]&&![aMessage.senderDisplayName isEqualToString:[AppManager DisplayName]]) {
            [array addObject:aMessage.senderDisplayName];
        }
    }
    return array.firstObject;
}
-(JSQMessage*)latestMessage{
    return _messages.lastObject;
}
-(JSQMessagesAvatarImage*)receiverAvatar{
    return [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:[NSString stringWithFormat:@"%c",[[self receiverDisplayName] characterAtIndex:0]] backgroundColor:[UIColor colorWithWhite:.5 alpha:1] textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:14] diameter:34];;
}
@end

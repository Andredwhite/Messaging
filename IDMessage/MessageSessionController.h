//
//  MessageSessionController.h
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Message;
@class MessageSession;
@interface MessageSessionController : NSObject
@property(readonly)NSInteger sessionCount;
-(MessageSession*)sessionAtIndex:(NSInteger)index;
-(void)deleteSessionAtIndex:(NSInteger)index;
-(void)newSessionWithMessage:(NSDictionary*)message andUsers:(NSArray*)users;
+(MessageSessionController*)sharedInstance;
@end

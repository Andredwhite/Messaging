//
//  ConversationViewController.m
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "ConversationViewController.h"
#import <JSQMessagesBubbleImage.h>
#import <JSQMessagesBubbleImageFactory.h>
#import "MessageSession.h"
#import "SharePresentationController.h"
#import "Message.h"
#import "MessageSessionController.h"
#import "ShareAnimationController.h"
#import "AppManager.h"
#import "ShareViewController.h"
#import "MediaViewViewController.h"
#import "OptionsViewController.h"
@interface ConversationViewController ()<JSQMessagesCollectionViewDelegateFlowLayout, JSQMessagesInputToolbarDelegate, JSQMessagesCollectionViewCellDelegate, UICollectionViewDataSource, UIViewControllerTransitioningDelegate, MediaSelect, UIPopoverPresentationControllerDelegate, OptionsButtonProtocol, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPress;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapRecognizer;
@property(retain, nonatomic)UICollectionReusableView* headerView;
@property(retain, nonatomic)NSIndexPath* indexPath;
@property(retain, nonatomic)NSMutableArray* users;
@end

@implementation ConversationViewController
- (IBAction)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p=[sender locationInView:self.collectionView];
    _indexPath=[self.collectionView indexPathForItemAtPoint:p];
    NSLog(@"Path is index %@",_indexPath);
    
    [self performSegueWithIdentifier:@"options" sender:[self.collectionView cellForItemAtIndexPath:_indexPath]];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([[AppManager sharedInstance].mainUser friendIDForName:textField.text]) {
        textField.textColor=[UIColor blueColor];
        [_users addObject:[[AppManager sharedInstance].mainUser friendIDForName:textField.text]];
    }
    return YES;
}

- (IBAction)screenTapped:(id)sender {
    for (UIView* subView in [_headerView.subviews objectAtIndex:0].subviews) {
        if ([subView isFirstResponder]) {
            [subView resignFirstResponder];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self.collectionView selector:@selector(reloadData) name:@"New Media" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reload) name:@"New Message" object:nil];
    
    if (_compose) {
        _users=[NSMutableArray new];
        [_users addObject:[AppManager UserID]];
        self.navigationItem.title=@"New Message";
        self.collectionView.collectionViewLayout.headerReferenceSize=CGSizeMake(0, 100);
        [self.collectionView addGestureRecognizer:_tapRecognizer];
       
        [self.collectionView registerNib:[UINib nibWithNibName:@"ToView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reuse"];
        
    }
    else{
        self.navigationItem.title=[self mainSession].receiverDisplayName;
         [self.collectionView addGestureRecognizer:_longPress];
    }
    // Do any additional setup after loading the view.
}

-(void)reload{
    [self.collectionView reloadData];
    NSIndexPath* path=[self mainSession].messageCount?[NSIndexPath indexPathForRow:[self mainSession].messageCount-1 inSection:0]:nil;
    if (!_compose&&path) {
        [self.collectionView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (_compose) {
        return CGSizeMake(0, 50);
    }
    return CGSizeZero;
    
}
- (UIView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView* view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                withReuseIdentifier:@"reuse"
                                                                       forIndexPath:indexPath];
    for (UIView* subView in [view.subviews objectAtIndex:0].subviews) {
        if ([subView isKindOfClass:[UITextField class]]) {
            [(UITextField*)subView setDelegate:self];
        }
    }
    _headerView=view;
    return view;
}
-(MessageSession*)mainSession{
    if (!_compose&&[MessageSessionController sharedInstance].sessionCount>0) {
        return [[MessageSessionController sharedInstance]sessionAtIndex:_indexOfSession];
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString*)senderId{
    return [AppManager UserID];
}
-(NSString*)senderDisplayName{
    return [AppManager DisplayName];
}
-(void)didPressAccessoryButton:(UIButton *)sender{
    [self performSegueWithIdentifier:@"share" sender:nil];
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Highlight");
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JSQMessagesCollectionViewCell* cell=[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    cell.textView.textColor=[UIColor whiteColor];
    cell.messageBubbleTopLabel.text=[[self mainSession]messageAtIndex:indexPath.row].senderDisplayName;
    return cell;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self mainSession].messageCount;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath{
    Message* message=[[self mainSession]messageAtIndex:indexPath.row];
    if (message.isMediaMessage) {
        if ([message.media isKindOfClass:[JSQPhotoMediaItem class]]) {
            [self performSegueWithIdentifier:@"MediaView" sender:[(JSQPhotoMediaItem*)message.media image]];
        }
    }
    
}
-(void)imageSelected:(UIImage *)theImage{
    NSDateFormatter* formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"MM/dd/yy HH:mm:ss";
    JSQPhotoMediaItem* media=[[JSQPhotoMediaItem alloc]initWithImage:theImage];
    if (!_compose) {
        [[self mainSession]addMessage:@{@"senderID":[self senderId],@"senderDisplay":[self senderDisplayName],@"date":[formatter stringFromDate:[NSDate date]],@"message":@"", @"media":media}];
        _indexOfSession=0;
        [self finishSendingMessage];
    }
    else{
        UIView* view=[_headerView.subviews objectAtIndex:0];
        for (UIView* theView in view.subviews) {
            if ([theView isKindOfClass:[UITextField class]]) {
                UITextField* field=(UITextField*)theView;
                if (field.text.length!=0) {//and the text is a user
                    //make new session and set it as the main session.
                    //add Message to session.
                    //reconfigure navigation stack...
                    [[MessageSessionController sharedInstance]newSessionWithMessage:@{@"senderID":[self senderId],@"senderDisplay":[self senderDisplayName],@"date":[formatter stringFromDate:[NSDate date]],@"message":@"", @"media":media}andUsers:_users];
                    _indexOfSession=0;
                    _compose=NO;
                    self.navigationItem.title=field.text;
                    [self finishSendingMessage];
                    
                }
            }
        }
        
    }
}

-(void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date{
    NSDateFormatter* formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"MM/dd/yy HH:mm:ss";
    if (_compose) {
        if (text.length!=0) {
            UIView* view=[_headerView.subviews objectAtIndex:0];
            for (UIView* theView in view.subviews) {
                if ([theView isKindOfClass:[UITextField class]]) {
                    UITextField* field=(UITextField*)theView;
                    if (field.text.length!=0) {//and the text is a user
                        //make new session and set it as the main session.
                        //add Message to session.
                        //reconfigure navigation stack...
                        
                        [[MessageSessionController sharedInstance]newSessionWithMessage:@{@"senderID":senderId,@"senderDisplay":senderDisplayName,@"date":[formatter stringFromDate:date],@"message":text,@"media":@"No"} andUsers:_users];
                        _indexOfSession=0;
                        _compose=NO;
                        self.navigationItem.title=field.text;
                        [self finishSendingMessage];
                        
                    }
                }
            }
        }
    }
    else{
        [[self mainSession] addMessage:@{@"senderID":senderId,@"senderDisplay":senderDisplayName,@"date":[formatter stringFromDate:date],@"message":text, @"media":@"No"}];
        _indexOfSession=0;
        [self finishSendingMessage];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"New Message" object:nil];
    }
    
    
}

-(id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [[self mainSession] messageAtIndex:indexPath.row];
}
-(void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath{
    [[self mainSession] deleteMessageAtIndex:indexPath.row];
}

-(id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[self mainSession] messageBubbleForIndex:indexPath.row];
}
-(id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [[self mainSession] avatarForIndex:indexPath.row];
}
-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath{
    return 20;
}
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return [[ShareAnimationController alloc] init];
}
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    SharePresentationController* controller=[[SharePresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
    return controller;
}
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}
-(void)deletePressed{
    [[self mainSession] deleteMessageAtIndex:_indexPath.row];
    [self.collectionView reloadData];
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"share"]) {
        segue.destinationViewController.transitioningDelegate=self;
        segue.destinationViewController.modalPresentationStyle=UIModalPresentationCustom;
        if ([segue.destinationViewController respondsToSelector:@selector(setDelegate:) ]) {
            [segue.destinationViewController performSelector:@selector(setDelegate:) withObject:self];
            }
    }
    else if ([segue.identifier isEqualToString:@"options"]){
        OptionsViewController* controller=segue.destinationViewController;
        JSQMessagesCollectionViewCell* cell=sender;
        controller.preferredContentSize=CGSizeMake(cell.bounds.size.width, 40);
        controller.modalPresentationStyle=UIModalPresentationPopover;
        UIPopoverPresentationController* presentation= segue.destinationViewController.popoverPresentationController;
        presentation.permittedArrowDirections=UIPopoverArrowDirectionAny;
        presentation.delegate=self;
        presentation.sourceView=[(UICollectionViewCell*)sender contentView];
        presentation.sourceRect=[(UICollectionViewCell*)sender contentView].frame;
        controller.delegate=self;
    }
    else{
        MediaViewViewController* controller=segue.destinationViewController;
        controller.mediaImage=sender;
    }
}
@end

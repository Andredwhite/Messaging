//
//  LoginViewController.m
//  IDMessage
//
//  Created by Andre White on 7/18/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "LoginViewController.h"
typedef NS_ENUM(NSInteger, IDLoginButtonType){
    IDBUTTONTYPEEMAIL=0,
    IDBUTTONTYPEGOOGLE,
    IDBUTTONTYPEFACEBOOK,
    IDBUTTONREGISTER
};
@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _usernameField.text=@"test1@test.com";
    _passwordField.text=@"tester";
    /*
    id handler=[[FIRAuth auth]addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        if ([auth currentUser]) {
            auth setValue:@"My name" forKey:@"displayName"
            [self performSegueWithIdentifier:@"signIn" sender:nil];
        }
    }];*/
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)screenTapped:(id)sender {
    if ([_usernameField isFirstResponder]) {
        [_usernameField resignFirstResponder];
    }
    else if([_passwordField isFirstResponder]){
        [_passwordField resignFirstResponder];
    }
}
- (IBAction)login:(UIButton *)sender {
    switch (sender.tag) {
        case IDBUTTONTYPEEMAIL:{
            [[FIRAuth auth]signInWithEmail:_usernameField.text password:_passwordField.text completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                if (!error) {
                    [self performSegueWithIdentifier:@"signIn" sender:nil];
                }
                else{
                    UIAlertController* controller=[UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                    [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }]];
                    [self presentViewController:controller animated:YES completion:nil];
                    NSLog(@"%@", error.localizedDescription);
                }
            }];
        }
        break;
        case IDBUTTONTYPEGOOGLE:
        //
        break;
        case IDBUTTONTYPEFACEBOOK:
        //
        break;
        case IDBUTTONREGISTER:{
            [[FIRAuth auth]createUserWithEmail:_usernameField.text password:_passwordField.text completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                if (!error) {
                    [self performSegueWithIdentifier:@"signIn" sender:nil];
                }
                else{
                    UIAlertController* controller=[UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                    [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }]];
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }];}
        default:
        break;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

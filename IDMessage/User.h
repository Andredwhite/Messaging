//
//  User.h
//  IDMessage
//
//  Created by Andre White on 7/23/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(readonly)NSString* displayName;
@property(readonly)NSString* userID;
-(instancetype)initWithUserId:(NSString*)userID;
@end

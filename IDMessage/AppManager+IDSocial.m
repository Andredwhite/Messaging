//
//  AppManager+IDSocial.m
//  IDMessage
//
//  Created by Andre White on 7/20/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "AppManager+IDSocial.h"

@implementation AppManager (IDSocial)

+(void)findFriend:(NSString*)friendName withCompletion:(void(^) (NSString* userID))completion{
    FIRDatabaseReference* ref=[AppManager UserIDReferenceForUserWithUserName:friendName];
    [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if ([snapshot.value isKindOfClass:[NSString class]]) {
                if (completion) {
                    completion(snapshot.value);
                }
                return;
            }
        else{
            if (completion) {
                completion(nil);
            }
        }
        
    }];
}
+(void)requestFriend:(NSString *)friendName withCompletion:(void (^)(BOOL success, NSError * error))completion{
    [self findFriend:friendName withCompletion:^(NSString *userID) {
        /*if (userID) {
            [[AppManager FriendRequestReferenceForUserID:userID ]updateChildValues:@{[AppManager DisplayName]:[AppManager UserID]}];
            if (completion) {
                completion(YES,nil);
            }
        }
        else{
            if (completion) {
                completion(NO,[NSError errorWithDomain:@"Friend Not Found" code:00 userInfo:nil]);
            }
        }*/
    }];
}
+(void)acceptFriend:(NSString*)friendName andID:(NSString*)friendID{
    FIRDatabaseReference* ref=[[FIRDatabase database]reference];
    NSDictionary* updates=@{[NSString stringWithFormat:[NSString userFriendsPathFormatString],[AppManager UserID] ]:@{friendName:friendID},
                            [NSString stringWithFormat:[NSString userFriendsPathFormatString],friendID ]:@{[AppManager DisplayName]:[AppManager UserID]}};
    [ref updateChildValues:updates];
    //[AppManager FriendRequestReferenceForUserID:[self userID]]
}
@end

//
//  ShareAnimationController.h
//  IDMessage
//
//  Created by Andre White on 7/15/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

@end

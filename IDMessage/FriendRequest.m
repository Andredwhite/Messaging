//
//  FriendRequest.m
//  IDMessage
//
//  Created by Andre White on 7/22/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "FriendRequest.h"
@interface FriendRequest ()
@property(retain, nonatomic)NSString* theSender;
@property(retain, nonatomic)NSString* theRecipient;
@property(retain, nonatomic)NSString* requestID;
@end
@implementation FriendRequest
-(instancetype)initWithKey:(NSString *)requestKey andInfo:(NSDictionary*)info{
    self=[super init];
    if (self) {
        _requestID=requestKey;
        _theRecipient=info[@"recipientID"];
        _theSender=info[@"senderID"];
    }
    return self;
}
-(NSString*)senderID{
    return _theSender;
}
-(NSString*)recipientID{
    return _theRecipient;
}
-(void)accept{
    if ([_theSender isEqualToString:[AppManager UserID]]) {
        [[AppManager FriendRequestReferenceWithID:_requestID]updateChildValues:@{@"accepted":@(YES)}];
        [[[AppManager UserFriendRequestReference]child:_requestID]removeValue];
    }
}
-(void)decline{
    if ([_theSender isEqualToString:[AppManager UserID]]) {
        [[AppManager FriendRequestReferenceWithID:_requestID]removeValue];
        [[[AppManager UserFriendRequestReference]child:_requestID]removeValue];
    }
}
@end

//
//  ShareViewController.h
//  IDMessage
//
//  Created by Andre White on 7/15/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MediaSelect <NSObject>

-(void)imageSelected:(UIImage*)theImage;

@end
@interface ShareViewController : UIViewController
@property(weak, nonatomic)id<MediaSelect>delegate;
@end

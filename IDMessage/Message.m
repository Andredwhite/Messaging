//
//  Message.m
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "Message.h"
@implementation Message
-(instancetype)initWithDictionary:(NSDictionary *)messageDictionary{
    NSDateFormatter* formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"MM/dd/yy HH:mm:ss";
    if ([messageDictionary[@"media"] isEqualToString:@"No"]) {
        self=[super initWithSenderId:messageDictionary[@"senderID"] senderDisplayName:messageDictionary[@"senderDisplay"] date:[formatter dateFromString: messageDictionary[@"date"]] text:messageDictionary[@"message"]];
    }
    else{
        JSQPhotoMediaItem* photo=[[JSQPhotoMediaItem alloc]initWithImage:nil];
        if ([messageDictionary[@"senderID"]isEqualToString:[AppManager UserID]]) {
            photo.appliesMediaViewMaskAsOutgoing=YES;
        }
        else{
            photo.appliesMediaViewMaskAsOutgoing=NO;
        }
        self=[super initWithSenderId:messageDictionary[@"senderID"] senderDisplayName:messageDictionary[@"senderDisplay"] date:[formatter dateFromString:messageDictionary[@"date"]] media:photo];
    }
    if (self) {
        if (self.isMediaMessage) {
            FIRStorageReference* ref=[[FIRStorage storage]referenceForURL:messageDictionary[@"media"]];
            [ref dataWithMaxSize:8*1024*1024 completion:^(NSData * _Nullable data, NSError * _Nullable error) {
                if (!error) {
                    JSQPhotoMediaItem* item=(JSQPhotoMediaItem*) self.media;
                    item.image=[UIImage imageWithData:data];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"New Media" object:nil];
                }
                else
                NSLog(@"%@", error.localizedDescription);
            }];
        }
    }
    return self;
    
}

-(instancetype)initWithMediaDictionary:(NSDictionary*)messageDictionary{
    NSDateFormatter* formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"MM/dd/yy HH:mm:ss";
    self=[super initWithSenderId:messageDictionary[@"senderID"] senderDisplayName:messageDictionary[@"senderDisplay"] date:[NSDate date] media:messageDictionary[@"media"]];
    if (self) {
        //
    }
    return self;
}
-(NSDictionary*)toDict{
    NSDateFormatter* formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"MM/dd/yy HH:mm:ss";
    NSDictionary* messageDict=@{@"message":self.text,@"senderID":self.senderId, @"date":[formatter stringFromDate:self.date],@"senderDisplay":self.senderDisplayName, @"media":@"No"};
    return messageDict;
}
-(UIImage*)pic{
    JSQPhotoMediaItem* media=(JSQPhotoMediaItem*)self.media;
    return media.image;
}
@end

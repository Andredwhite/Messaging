//
//  FriendsViewTableViewController.m
//  IDMessage
//
//  Created by Andre White on 7/23/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "FriendsViewTableViewController.h"
#import "FriendsOptionsTableViewCell.h"
@interface FriendsViewTableViewController ()<UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(retain, nonatomic)NSMutableArray* results;
@end

@implementation FriendsViewTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_selectionIndex==1) {
        _searchBar.hidden=NO;
        _searchBar.delegate=self;
        _results=[NSMutableArray new];
    }
    else{
        self.tableView.tableHeaderView=nil;
    }
    self.tableView.tableFooterView=[[UIView alloc]init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateTable) name:@"FriendsLoaded" object:nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)updateTable{
    if (_selectionIndex==0) {
        [self.tableView reloadData];
    }
}
- (IBAction)screenTapped:(id)sender {
    if ([_searchBar isFirstResponder]) {
        [_searchBar resignFirstResponder];
    }
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [_results removeAllObjects];
    [AppManager findFriend:searchBar.text withCompletion:^(NSString *userID) {
        if (userID) {
            [_results addObject:searchBar.text];
        }
        else{
            [_results addObject:@"No Results 🤦🏾‍♂️"];
        }
        [self.tableView reloadData];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSArray*)dataSourceArray{
    switch (_selectionIndex) {
        case 0:
            return [AppManager sharedInstance].mainUser.friends;
            break;
        case 1:
            return _results;
        case 2:
            return [AppManager sharedInstance].mainUser.friendRequests;
        default:
            return 0;
            break;
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self dataSourceArray].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendsOptionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    if (_selectionIndex==1) {
        cell.titleLabel.text=[_results objectAtIndex:indexPath.row];
    }
    else{
        cell.titleLabel.text=[[[self dataSourceArray]objectAtIndex:indexPath.row] displayName];
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  FriendRequest.h
//  IDMessage
//
//  Created by Andre White on 7/22/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface FriendRequest : NSObject
@property(readonly)NSString* senderID;
@property(readonly)NSString* recipientID;
-(instancetype)initWithKey:(NSString*)requestKey andInfo:(NSDictionary*)info;
-(instancetype)initWithKey:(NSString*)requestKey;
-(void)accept;
-(void)decline;
@end

//
//  MediaViewViewController.h
//  IDMessage
//
//  Created by Andre White on 7/16/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaViewViewController : UIViewController
@property(retain, nonatomic)UIImage* mediaImage;

@end

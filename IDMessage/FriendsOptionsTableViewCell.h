//
//  FriendsOptionsTableViewCell.h
//  IDMessage
//
//  Created by Andre White on 7/22/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsOptionsTableViewCell : UITableViewCell
@property(weak, nonatomic)IBOutlet UILabel* titleLabel;
@end

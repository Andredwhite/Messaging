//
//  AllSessionsTableViewController.m
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "AllSessionsTableViewController.h"
#import "MessageSessionController.h"
#import "ConversationViewController.h"
#import "SessionTableViewCell.h"
#import "SharePresentationController.h"
@interface AllSessionsTableViewController ()<UIViewControllerTransitioningDelegate>

@end

@implementation AllSessionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   if ([FIRAuth auth].currentUser.displayName.length==0) {
        [self performSegueWithIdentifier:@"setDisplayName" sender:nil];
   }
    [[NSNotificationCenter defaultCenter]addObserver:self.tableView selector:@selector(reloadData) name:@"New Message" object:nil];
    self.tableView.tableFooterView=nil;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
    return [MessageSessionController sharedInstance].sessionCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    MessageSession* session=[[MessageSessionController sharedInstance] sessionAtIndex:indexPath.row];
    [cell configureWithSession:session];
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[MessageSessionController sharedInstance] deleteSessionAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
-(UIPresentationController*)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    DisplayNamePresentationController* controller=[[DisplayNamePresentationController alloc]initWithPresentedViewController:presented presentingViewController:presenting];
    
    return controller;
}
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"setDisplayName"]) {
        segue.destinationViewController.modalPresentationStyle=UIModalPresentationCustom;
        segue.destinationViewController.transitioningDelegate=self;
        //
    }
    else if ([segue.identifier isEqualToString:@"Friends"]){
        
    }
    else{
        if ([sender isKindOfClass:[UITableViewCell class]]) {
            ConversationViewController* dest=segue.destinationViewController;
            dest.indexOfSession=[self.tableView indexPathForCell:sender].row;
            dest.compose=NO;
        }
        else{
            ConversationViewController* dest=segue.destinationViewController;
            dest.collectionView.dataSource=nil;
            dest.compose=YES;
            
        }
    }
    

    
}

@end

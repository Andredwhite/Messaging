//
//  OptionsViewController.m
//  IDMessage
//
//  Created by Andre White on 7/17/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "OptionsViewController.h"

@interface OptionsViewController ()

@end

@implementation OptionsViewController
@synthesize delegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //self.preferredContentSize=CGSizeMake(320, 20);
}
- (IBAction)deleteButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [delegate deletePressed];
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

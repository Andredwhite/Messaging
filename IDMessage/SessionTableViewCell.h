//
//  SessionTableViewCell.h
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageSession.h"
@interface SessionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *displayName;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UILabel *date;
-(void)configureWithSession:(MessageSession*)theSession;
@end

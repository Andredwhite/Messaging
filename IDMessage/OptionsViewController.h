//
//  OptionsViewController.h
//  IDMessage
//
//  Created by Andre White on 7/17/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OptionsButtonProtocol <NSObject>

-(void)deletePressed;

@end
@interface OptionsViewController : UIViewController
@property(weak, nonatomic)id<OptionsButtonProtocol>delegate;
@end

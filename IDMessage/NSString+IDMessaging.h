//
//  NSString+IDMessaging.h
//  IDMessage
//
//  Created by Andre White on 7/20/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (IDMessaging)
+(NSString*)userConversationsPathFormatString;
+(NSString*)conversationsPathFormatString;
+(NSString*)userFriendsPathFormatString;
+(NSString*)groupsPathFormatString;
+(NSString*)userFriendRequestFormatString;
+(NSString*)userFriendsAddPathFormatString;
+(NSString*)ConversationsHandleKey;
+(NSString*)AllConversationsHandleKey;
+(NSString*)AllFriendsHandleKey;
+(NSString*)FriendRequestsHandle;
+(NSString*)userNamesForIDPathFormatString;
+(NSString*)IDforUserNamesPathFormatString;
@end

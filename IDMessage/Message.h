//
//  Message.h
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSQMessage.h>
#import <JSQPhotoMediaItem.h>
#import <JSQLocationMediaItem.h>
#import <JSQVideoMediaItem.h>
@interface Message : JSQMessage
@property(readonly)UIImage* pic;
-(instancetype)initWithDictionary:(NSDictionary*)messageDictionary;
-(instancetype)initWithMediaDictionary:(NSDictionary*)messageDictionary;
-(NSDictionary*)toDict;
@end

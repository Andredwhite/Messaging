//
//  AppDelegate.h
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


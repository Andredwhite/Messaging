//
//  FriendsOptionsTableViewCell.m
//  IDMessage
//
//  Created by Andre White on 7/22/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "FriendsOptionsTableViewCell.h"

@implementation FriendsOptionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

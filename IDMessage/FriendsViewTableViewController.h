//
//  FriendsViewTableViewController.h
//  IDMessage
//
//  Created by Andre White on 7/23/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewTableViewController : UITableViewController
@property(assign, nonatomic)NSInteger selectionIndex;
@end

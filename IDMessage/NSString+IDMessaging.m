//
//  NSString+IDMessaging.m
//  IDMessage
//
//  Created by Andre White on 7/20/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "NSString+IDMessaging.h"

@implementation NSString (IDMessaging)
/*!
 @brief @"UserNames/%@/"
 */
+(NSString*)userNamesForIDPathFormatString{
    return @"UserNamesForID/%@/";
}
+(NSString*)IDforUserNamesPathFormatString{
    return @"IDForUserName/%@";
}
/*!
 @brief @"Users/%@/Conversations/"
 */
+(NSString*)userConversationsPathFormatString{
    return @"Users/%@/Conversations/";
}
/*!
 @brief @"Friends/%@/"
 */
+(NSString*)userFriendsPathFormatString{
    return @"Friends/%@/";
}
/*!
 @brief Conversations/%@/
 */
+(NSString*)conversationsPathFormatString{
    return @"Conversations/%@/";
}
/*!
 @brief @"Groups/%@/"
 */
+(NSString*)groupsPathFormatString{
    return @"Groups/%@/";
}
/*!
 @brief @"Users/%@/Requests/"
 */
+(NSString*)userFriendRequestFormatString{
    return @"Users/%@/Requests/";
}
+(NSString*)userFriendsAddPathFormatString{
    return [[NSString userFriendsPathFormatString]stringByAppendingString:@"%@"];
}
+(NSString*)ConversationsHandleKey{
    return @"ConversationsHandle";
}
+(NSString*)AllConversationsHandleKey{
    return @"AllConversationsHandle";
}
+(NSString*)AllFriendsHandleKey{
    return @"AllFriendsHandle";
}
+(NSString*)FriendRequestsHandle{
    return @"FriendRequestsHandle";
}
@end

//
//  AppManager+IDSocial.h
//  IDMessage
//
//  Created by Andre White on 7/20/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "AppManager.h"

@interface AppManager (IDSocial)
+(void)findFriend:(NSString*)friendName withCompletion:(void(^) (NSString* userID))completion;
+(void)requestFriend:(NSString*)friendName withCompletion:(void(^) (BOOL success, NSError* error))completion;
+(void)acceptFriend:(NSString*)friendName andID:(NSString*)friendID;
@end

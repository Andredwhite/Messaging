//
//  SessionTableViewCell.m
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "SessionTableViewCell.h"

@implementation SessionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configureWithSession:(MessageSession*)theSession{
    
    self.displayName.text=theSession.receiverDisplayName;
    self.avatarImage.image=theSession.receiverAvatar.avatarImage;
    self.message.text=theSession.latestMessage.text;
    NSDateFormatter* formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"MM/dd/yy HH:mm:ss";
    self.date.text=[formatter stringFromDate:theSession.latestMessage.date];
}
@end

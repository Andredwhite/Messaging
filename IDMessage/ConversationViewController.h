//
//  ConversationViewController.h
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>

@interface ConversationViewController : JSQMessagesViewController
@property(assign, nonatomic)NSInteger indexOfSession;
@property(assign, nonatomic)BOOL compose;
@end

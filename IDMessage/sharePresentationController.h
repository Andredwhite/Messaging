//
//  SharePresentationController.h
//  IDMessage
//
//  Created by Andre White on 7/15/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharePresentationController : UIPresentationController
@property(retain, nonatomic)UIView* dimmingView;
@end

//
//  MessageSessionController.m
//  IDMessage
//
//  Created by Andre White on 7/8/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "MessageSessionController.h"

static MessageSessionController* theSharedInstance;
@interface MessageSessionController ()
@property(retain, nonatomic)NSMutableArray* sessions;
@property(retain, nonatomic)NSMutableDictionary* convoCache;

@end
@implementation MessageSessionController
-(instancetype)initPrivate{
    self=[super init];
    if (self) {
        _convoCache=[[NSMutableDictionary alloc]init];
        _sessions=[[NSMutableArray alloc]init];
        FIRDatabaseHandle handle=[[AppManager UserConversationsReference] observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            MessageSession* session=[[MessageSession alloc]initWithConvoKey:snapshot.key];
            [_sessions addObject:session];
            if ([_convoCache objectForKey:snapshot.key]) {
                [session addMessage:[_convoCache objectForKey:snapshot.key]];
                [_convoCache removeObjectForKey:snapshot.key];
            }
            [[NSNotificationCenter defaultCenter]postNotificationName:@"New Message" object:nil];
            
        }];
        [[AppManager sharedInstance]addHandle:handle ForKey:[NSString AllConversationsHandleKey]];
    }
    return self;
}
+(MessageSessionController*)sharedInstance{
    if (!theSharedInstance) {
        theSharedInstance=[[MessageSessionController alloc]initPrivate];
    }
    return theSharedInstance;
}
-(void)deleteSessionAtIndex:(NSInteger)index{
    [self sortSessions];
    [_sessions removeObjectAtIndex:index];
}
-(MessageSession*)sessionAtIndex:(NSInteger)index{
    [self sortSessions];
    return [_sessions objectAtIndex:index];
}
-(NSInteger)sessionCount{
    return _sessions.count;
}
-(void)newSessionWithMessage:(NSDictionary *)message andUsers:(NSArray *)users{
    if (![message[@"media"] isKindOfClass:[NSString class]]) {
        NSString* convoKey=[[AppManager ConversationsReference] childByAutoId].key;
        NSString* messageKey=[[[AppManager ConversationsReference]child:convoKey]childByAutoId].key;
        [_convoCache setObject:message forKey:convoKey];
        [[[AppManager ConversationsReference] child:convoKey]setValue:messageKey];
        [self addConvoKey:convoKey forUsers:users];
    }
    else{
        NSString* convoKey=[[AppManager ConversationsReference] childByAutoId].key;
        NSString* messagekey=[[[AppManager ConversationsReference] child:convoKey]childByAutoId].key;
        [[[[AppManager ConversationsReference] child:convoKey]child:messagekey]setValue:message];
        [self addConvoKey:convoKey forUsers:users];
    }
    
}
-(void)addConvoKey:(NSString*)convoKey forUsers:(NSArray*)users{
    for (NSString* userID in users) {
        NSString* path=[NSString stringWithFormat:@"%@/%@",[NSString stringWithFormat:[NSString userConversationsPathFormatString],userID],convoKey];
        [[AppManager dataBaseReferenceWithPath:path]setValue:users];
    }
}
-(void)sortSessions{
    NSSortDescriptor* desc=[NSSortDescriptor sortDescriptorWithKey:@"latestMessage.date" ascending:NO];
    [_sessions sortUsingDescriptors:@[desc]];
}
@end

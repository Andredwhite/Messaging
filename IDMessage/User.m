//
//  User.m
//  IDMessage
//
//  Created by Andre White on 7/23/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "User.h"
@interface User ()
@property(retain, nonatomic)NSString* theUserID;
@property(retain, nonatomic)NSString* theDisplayName;
@end
@implementation User
-(instancetype)initWithUserId:(NSString *)userID{
    self=[super init];
    if (self) {
        _theUserID=userID;
        [[AppManager UserNamesReferenceForUserWithID:userID] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            _theDisplayName=snapshot.value;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"FriendsLoaded" object:nil];
        }];
    }
    return self;
}
-(NSString*)displayName{
    return _theDisplayName;
}
-(NSString*)userID{
    return _theUserID;
}
@end

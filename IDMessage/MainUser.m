//
//  MainUser.m
//  IDMessage
//
//  Created by Andre White on 7/23/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "MainUser.h"
#import "FriendRequest.h"
@interface MainUser ()
@property(retain, nonatomic)NSMutableArray* friends;
@property(retain, nonatomic)NSMutableArray* friendRequests;
@end
@implementation MainUser
-(instancetype)init{
    self=[super init];
    if (self) {
        _friends=[NSMutableArray new];
        FIRDatabaseHandle handle1=[[AppManager UserFriendsReference] observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            [_friends addObject:[[User alloc]initWithUserId:snapshot.value]];
            
        }];
        _friendRequests=[NSMutableArray new];
        FIRDatabaseHandle handle2=[[AppManager UserFriendRequestReference]observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            [_friendRequests addObject:[[FriendRequest alloc]initWithKey:snapshot.value]];
        }];
        [[AppManager sharedInstance]addHandle:handle1 ForKey:[NSString AllFriendsHandleKey]];
        [[AppManager sharedInstance]addHandle:handle2 ForKey:[NSString FriendRequestsHandle]];
    }
    return self;
}
-(NSString*)userID{
    return [FIRAuth auth].currentUser.uid;
}
-(NSString*)displayName{
    return [FIRAuth auth].currentUser.displayName;
}
-(NSString*)friendIDForName:(NSString*)displayName{
    NSPredicate* predicate=[NSPredicate predicateWithFormat:@"displayName==%@",displayName];
    User* friend=[_friends filteredArrayUsingPredicate:predicate].firstObject;
    if (friend) {
        return friend.userID;
    }
    return nil;
}
@end

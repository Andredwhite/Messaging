//
//  MainUser.h
//  IDMessage
//
//  Created by Andre White on 7/23/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "User.h"

@interface MainUser : User
@property(readonly)NSArray* friends;
@property(readonly)NSArray* friendRequests;
/*!
 @brief Use this method to check if the friendName is an actual friend in the users friends list.
 */
-(NSString*)friendIDForName:(NSString*)displayName;
@end

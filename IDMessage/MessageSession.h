//
//  MessageSession.h
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JSQMessagesBubbleImage;
@class JSQMessagesAvatarImage;
@interface MessageSession : NSObject
@property(readonly)NSInteger messageCount;
@property(readonly)NSString* receiverDisplayName;
@property(readonly)JSQMessage* latestMessage;
@property(readonly)JSQMessagesAvatarImage* receiverAvatar;
-(instancetype)initWithConvoKey:(NSString*)convoKey;
-(Message*)messageAtIndex:(NSInteger)index;
-(void)deleteMessageAtIndex:(NSInteger)index;
-(void)addMessage:(NSDictionary*)theMessage;
-(JSQMessagesBubbleImage*)messageBubbleForIndex:(NSInteger)index;
-(JSQMessagesAvatarImage*)avatarForIndex:(NSInteger)index;
@end

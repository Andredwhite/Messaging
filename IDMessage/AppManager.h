//
//  AppManager.h
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//
@import FirebaseDatabase;
@import FirebaseStorage;
#import <Foundation/Foundation.h>

@interface AppManager : NSObject
@property(readonly)MainUser* mainUser;
-(NSString*)friendForName:(NSString*)friendName;
-(void)addHandle:(FIRDatabaseHandle)handle ForKey:(NSString*)key;
-(void)removeHandleForKey:(NSString*)key;
+(NSString*)UserID;
+(NSString*)DisplayName;
+(AppManager*)sharedInstance;
+(FIRDatabaseReference*)UserNamesReferenceForUserWithID:(NSString*)userId;
+(FIRDatabaseReference*)UserIDReferenceForUserWithUserName:(NSString*)userName;
+(FIRDatabaseReference*)UserConversationsReference;
+(FIRDatabaseReference*)ConversationsReference;
+(FIRDatabaseReference*)UserFriendsReference;
+(FIRDatabaseReference*)FriendRequestReferenceWithID:(NSString*)requestID;
+(FIRDatabaseReference*)UserFriendRequestReference;
+(FIRDatabaseReference*)dataBaseReferenceWithPath:(NSString*)path;
+(FIRDatabaseReference*)ConversationReferenceWithKey:(NSString*)key;
+(FIRStorageReference*)ConversationStorageReferenceWithKey:(NSString *)key;
+(FIRDatabaseReference*)FriendRequestReferenceForUserWithID:(NSString*)userID;
+(FIRDatabaseReference*)UserConversationsReferenceForUser:(NSString*)userID;
+(FIRDatabaseReference*)AddFriendsReferenceWithIndex:(NSNumber*)index;

@end

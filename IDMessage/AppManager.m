//
//  AppManager.m
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import "AppManager.h"

static AppManager* theSharedInstance;
@interface AppManager ()

@property(retain, nonatomic)NSMutableDictionary* friends;
@property(retain, nonatomic)NSMutableDictionary* observerHandleHash;
@property(retain, nonatomic)MainUser* mainUser;
@end
@implementation AppManager

-(instancetype)initPrivate{
    self=[super init];
    if (self) {
        _observerHandleHash=[NSMutableDictionary new];
    }
    return self;
}
-(void)addHandle:(FIRDatabaseHandle)handle ForKey:(NSString *)key{
    [_observerHandleHash setObject:@(handle) forKey:key];
}
-(void)removeHandleForKey:(NSString *)key{
    FIRDatabaseHandle handle=[_observerHandleHash[key] unsignedIntegerValue];
    [[[FIRDatabase database]reference]removeObserverWithHandle:handle];
    [_observerHandleHash removeObjectForKey:key];
}
-(MainUser*)mainUser{
    if (!_mainUser) {
        _mainUser=[[MainUser alloc]init];
    }
    return _mainUser;
}

/*!
 @brief Use this method to get the current signed in UserID
 */
+(NSString*)UserID{
    return [FIRAuth auth].currentUser.uid;
}
/*!
 @brief Use this method to get the current signed in Users' DisplayName
*/
+(NSString*)DisplayName{
    return [FIRAuth auth].currentUser.displayName;
}
/*!
 @brief Use this method to access an instance of AppManager. 
 */
+(AppManager*)sharedInstance{
    if (!theSharedInstance) {
        theSharedInstance=[[AppManager alloc]initPrivate];
    }
    return theSharedInstance;
}
/*!
 @brief Method to quickly get a database Reference with a given NSString path. Equivalent to calling [[FIRDatabase database]referenceWithPath:path]
 */
+(FIRDatabaseReference*)dataBaseReferenceWithPath:(NSString*)path{
    return [[FIRDatabase database]referenceWithPath:path];
}
/*!
 @brief Main Database Conversation Reference for given conversation key. This location will have an array of messages for the given conversation key. Conversations/key
 */
+(FIRDatabaseReference*)ConversationReferenceWithKey:(NSString *)key{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString conversationsPathFormatString],key]];
}
/*!
 @brief App users Conversations reference. This location will have the array of dictionaries where the key is a conversation ID and the value is an array of users for that conversation. "Users/userID/Conversations/"
 */
+(FIRDatabaseReference*)UserConversationsReference{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString userConversationsPathFormatString],[AppManager UserID]]];
}
/*!
 @brief Method to quickly get a storage Reference with a given NSString path. Equivalent to calling [[FIRStorage storage]referenceWithPath:path]
 */
+(FIRStorageReference*)storageReferenceWithPath:(NSString*)path{
    return [[FIRStorage storage]referenceWithPath:path];
}
/*!
 @brief Main Database Conversation Storage Reference for given conversation key. This location will have an array of media for the given conversation key. "Conversations/key/"
 */
+(FIRStorageReference*)ConversationStorageReferenceWithKey:(NSString *)key{
    return [AppManager storageReferenceWithPath:[NSString stringWithFormat:[NSString conversationsPathFormatString],key]];
}
/*!
  @brief Friend Request reference path for userID with given userID. Use this path to make friend request to a user with the userID given. "Users/userID/Friends/Requests/"
 */
+(FIRDatabaseReference*)FriendRequestReferenceForUserWithID:(NSString *)userID{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString userFriendRequestFormatString],userID]];
}
/*!
 
 @brief Main Database Conversation Reference. This location will have an array of messages. "Conversations/"
 */
+(FIRDatabaseReference*)ConversationsReference{
    return [AppManager dataBaseReferenceWithPath:@"Conversations/"];
}
/*!
 @brief Conversations reference for a given UserID. This location will have the array of dictionaries where the key is a conversation ID and the value is an array of users for that conversation. "Users/userID/Conversations/"
 */
+(FIRDatabaseReference*)UserConversationsReferenceForUser:(NSString*)userID{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString userConversationsPathFormatString],userID]];
}
/*!
 @brief UserName Database Reference. Use this to set (find, or update) the userName for a user.
 */
+(FIRDatabaseReference*)UserNamesReferenceForUserWithID:(NSString*)userId{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString userNamesForIDPathFormatString],userId]];
}
+(FIRDatabaseReference*)UserIDReferenceForUserWithUserName:(NSString*)userName{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString IDforUserNamesPathFormatString],userName]];
}
/*!
 @brief Friends reference for currently signed in User.
 */
+(FIRDatabaseReference*)UserFriendsReference{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString userFriendsPathFormatString],[AppManager UserID]]];
}
/*!
 @brief Friend Request reference for currently signed in User. 
 */
+(FIRDatabaseReference*)UserFriendRequestReference{
    return [AppManager FriendRequestReferenceForUserWithID:[AppManager UserID]];
}
/*!
 @brief Friend Request reference for database.
 */
+(FIRDatabaseReference*)FriendRequestReferenceWithID:(NSString*)requestID{
    return [AppManager dataBaseReferenceWithPath:[@"Requests/" stringByAppendingString:requestID]];
}
+(FIRDatabaseReference*)AddFriendsReferenceWithIndex:(NSNumber*)index{
    return [AppManager dataBaseReferenceWithPath:[NSString stringWithFormat:[NSString userFriendsAddPathFormatString],[AppManager UserID],index]];
}
@end

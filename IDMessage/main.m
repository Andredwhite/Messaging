//
//  main.m
//  IDMessage
//
//  Created by Andre White on 7/7/17.
//  Copyright © 2017 Andre White. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
